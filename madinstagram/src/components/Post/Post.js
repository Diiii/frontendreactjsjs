import React, { Component } from "react";
import './Post.css';
import { IconButton } from '@material-ui/core';
import FavoriteBorderRoundedIcon from '@material-ui/icons/FavoriteBorderRounded';
import InsertCommentRoundedIcon from '@material-ui/icons/InsertCommentRounded';
import Grid from '@material-ui/core/Grid';
    class Post extends Component {
      constructor(props){
            super(props);
      }
      render() {
        const nickname = this.props.nickname;
        const avatar = this.props.avatar;
        const image = this.props.image;
        const caption = this.props.caption;

      return (
          <article className="Post">
            <div className="Post-user">
              <Grid container spacing={3} alignItems="center" justify="center">
                <Grid container item xs={12} spacing={0}>
                  <img className="Post-user-avatar img" src={avatar} alt={nickname} />
                  <span>{nickname}</span>
                </Grid>
                <Grid container item alignItems="center" justify="center">
                  <img alt={caption} className="Post-img img" src={image} />
                </Grid>
                <Grid container item xs={12} spacing={3}>
                  <strong>{nickname}</strong>{caption}
                </Grid>
                <Grid container item xs={12} spacing={3}>
                  <IconButton aria-label="beforelike" edge="start">
                    <FavoriteBorderRoundedIcon />
                  </IconButton>
                  <IconButton aria-label="comment" edge='end'>
                    <InsertCommentRoundedIcon />
                  </IconButton>
                </Grid>
              </Grid>
            </div>
          </article>
        );
      }
    }
    export default Post;