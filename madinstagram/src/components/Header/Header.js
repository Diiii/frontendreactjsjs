import React, {Component} from "react";
import "./Header.css";
import {AppBar, 
    Toolbar, 
    IconButton,
    Typography,
    InputBase
} 
from "@material-ui/core"
import SearchIcon from '@material-ui/icons/Search';
import { fade, makeStyles } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';
import {    
    AccountCircle
} from "@material-ui/icons"

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: grey[200],
    '&:hover': {
      backgroundColor: grey[200],
    },
    marginLeft: 5 ,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
    flexGrow : 1
  },
  searchIcon: {
    marginLeft: 5 ,
    //width: theme.spacing(50),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
compteIcon: {
  position: 'relative'
},

  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },
}));

const Header = () => {
  const classes = useStyles();
    return(
        <AppBar position="static" className="grow">
            <Toolbar>                   
                <IconButton edge="start" >
                           <a className="Nav-brand-logo" href="/">
                             Instagram
                           </a>
                </IconButton>
                        <div className={classes.search} >
                          <div className={classes.searchIcon}>
                            <SearchIcon />
                          </div>
                          <InputBase
                            placeholder="Search…"
                            classes={{
                              root: classes.inputRoot,
                              input: classes.inputInput,
                            }}

                           
                            inputProps={{ 'aria-label': 'search' }}
                          />
                        </div>
                    <IconButton edge="end" color="primary" className={classes.compteIcon}>
                      <AccountCircle />
                    </IconButton>
            </Toolbar>
        </AppBar>
    );
}


export default Header;
